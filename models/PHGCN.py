import torch.nn as nn
import torch.nn.functional as f
from dgl.nn.pytorch import GATConv
from base.graph_filtering_layers import GATConvEuclidean



class PHGCN(nn.Module):
    def __init__(self,
                 feat_repr_dims,
                 num_heads,
                 activation,
                 feat_dropout,
                 euclidean_attn,
                 lattice_neighbor_range,
                 global_attention,
                 exact_global_attention,
                 ignore_structure,
                 loc_dim,
                 attn_dropout,
                 max_lattice_decay,
                 exp_scale,
                 alpha,
                 residual):
        super().__init__()
        dims = list(zip(feat_repr_dims[:-1], feat_repr_dims[1:]))
        self.depth = len(feat_repr_dims) - 1
        self.feat_dropout = feat_dropout
        self.attn_dropout = attn_dropout
        self.activation = activation

        if len(num_heads) == 1 != self.depth:
            num_heads = num_heads * self.depth

        self.gats = nn.ModuleList()

        shared = dict(feat_drop=feat_dropout, attn_drop=attn_dropout)
        gat_layer = GATConvEuclidean if euclidean_attn else GATConv
        shared.update(
            dict(
                lattice_neighbor_range = lattice_neighbor_range,
                global_attention = global_attention,
                exact_global_attention = exact_global_attention,
                ignore_structure = ignore_structure,
                loc_dim = loc_dim,
                max_lattice_decay = max_lattice_decay,
                exp_scale = exp_scale
            ) if euclidean_attn else dict(negative_slope=alpha)
        )
        #When using lattice filtering together with structure based aggregation, the lattice features are concatenated to the normal (structural) GAT features
        hybrid_attn_multiplier = 2 if (global_attention and not(ignore_structure)) else 1 

        # input projection (no residual)
        self.gats.append(gat_layer(
            *dims[0],
            num_heads[0],
            residual=False,
            activation=self.activation,
            **shared,
        ))

        # hidden layers (due to multi-head, in_dim = num_hidden * num_heads)
        self.gats.extend([
            gat_layer(
                in_dim*num_heads[indx]*hybrid_attn_multiplier, out_dim,
                num_heads[indx+1],
                residual=residual,
                activation=self.activation,
                **shared,
            ) for indx, (in_dim, out_dim) in enumerate(dims[1:-1])
        ])

        # output readout (no activation)
        self.gats.append(gat_layer(
            dims[-1][0]*num_heads[-2]*hybrid_attn_multiplier, dims[-1][1],
            num_heads[-1],
            residual=residual,
            activation=None,
            **shared,
        ))

        self.criterion = nn.CrossEntropyLoss()

    def forward(self, graph, features):
        h = features
        for gat in self.gats[:-1]:
            h = gat(graph, h).flatten(1)
        # output projection
        logits = self.gats[-1](graph, h).mean(1)
        return logits

    def loss(self, targets, predictions):
        return self.criterion(predictions, targets)



    
