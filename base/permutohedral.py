import torch
import numpy as np
import torch.nn.functional as F
import torch.nn as nn


class lattice_op_batched (torch.autograd.Function):
    @staticmethod
    def forward(ctx,barycentric_indices_X,barycentric_indices_Y,barycentric_vals,
                           blur_projection,features_heads,loc_dim):
        splat_projections,combined_blurred_features,sliced_features = efficient_lattice_batched.filter(barycentric_indices_X,barycentric_indices_Y,barycentric_vals,blur_projection,features_heads,loc_dim)
        ctx.save_for_backward(*splat_projections,blur_projection,features_heads,combined_blurred_features)
        ctx.loc_dim = loc_dim
        return sliced_features        

    @staticmethod
    def backward(ctx, grad_output):
        splat_projections,blur_projection,features_heads,combined_blurred_features = [ctx.saved_tensors[:-3],*ctx.saved_tensors[-3:]]
        n_heads,n_points_per_head,feature_dim = features_heads.size()        
        loc_dim = ctx.loc_dim   
        DLDY = grad_output
        F = features_heads
        B = blur_projection
        BT = B.transpose(1,0)
        S = splat_projections
        ST = [x.transpose(1,0) for x in splat_projections]

        S_DLDY = [torch.sparse.mm(S[i],DLDY[i]) for i in range(n_heads)]
        BT_S_DLDY = [torch.sparse.mm(BT,S_DLDY[i]) for i in range(n_heads)]

        DLDF = torch.stack([torch.sparse.mm(ST[i],BT_S_DLDY[i]) for i in range(n_heads)])

        B_S_F = combined_blurred_features.view(-1,n_heads,feature_dim)

        barycentric_indices = [S[i]._indices() for i in range(n_heads)]
        # DLDS_part1 = torch.mm(BT_S_DLDY,F.transpose(1,0))
        # DLDS_part2 = torch.mm(B_S_F,DLDY.transpose(1,0))        

        # DLDS = DLDS_part1 + DLDS_part2
        # barycentric_grads = DLDS[barycentric_indices[0],barycentric_indices[1]]

        D1 = loc_dim + 1 #S._nnz() // features.size(0)
        DLDS_part1_direct = [(BT_S_DLDY[i][barycentric_indices[i][0]].view(-1,D1,feature_dim) * F[i].unsqueeze(1)).sum(-1).view(-1) for i in range(n_heads)]
        DLDS_part2_direct = [(B_S_F[:,i,:][barycentric_indices[i][0]].view(-1,D1,feature_dim) * DLDY[i].unsqueeze(1)).sum(-1).view(-1) for i in range(n_heads)]
        barycentric_grads = torch.cat(DLDS_part1_direct) + torch.cat(DLDS_part2_direct)


        

        return None,None,barycentric_grads,None,DLDF,None


class efficient_lattice_batched(nn.Module):
    def __init__(self,D,neighbor_range = 1,weight_foo = None):
        super(efficient_lattice_batched,self).__init__()
        self.D = D
        self.neighbor_range = neighbor_range
        self.weight_foo = weight_foo
   
        self.register_buffer('embedding_matrix',efficient_lattice_batched.make_embedding_matrix(D))
        self.register_buffer('canonical_simplex',efficient_lattice_batched.get_canonical_simplex(D))
        self.make_neighbor_deltas()

        
    def embed(self,locs):
        '''
        embed locs into the 1^T x = 0 plane
        '''
        return locs @ self.embedding_matrix

    def make_neighbor_deltas(self):
        '''
        calculates the integer deltas that need to be added to each lattice point to obtain its 2(D+1) neighbors
        '''

        D1 = self.D + 1
        base_deltas_half = torch.empty((D1,D1)).fill_(-1)
        base_deltas_half[torch.arange(D1),torch.arange(D1)] = self.D

        base_deltas = torch.cat((torch.zeros((1,D1)),base_deltas_half,base_deltas_half * -1)).long()

        current_deltas = base_deltas
        for i in range(self.neighbor_range-1):
            current_deltas = (current_deltas.unsqueeze(0) + base_deltas.unsqueeze(1)).view(-1,D1)
            current_deltas = torch.unique(current_deltas,dim = 0)

        if self.weight_foo is None:
            neighbor_weights = torch.ones(current_deltas.size(0)).float()
        else:    
            neighbor_weights = self.weight_foo(current_deltas)

        self.register_buffer('neighbor_deltas',current_deltas)
        self.register_buffer('neighbor_weights', neighbor_weights)


    def forward(self,locs_heads,features_heads,use_custom_op = True):
        locs = locs_heads.reshape(-1,self.D)
        embedded_locs = self.embed(locs)
        D1 = self.D + 1
        n_points = embedded_locs.size(0)
        n_heads,n_points_per_head,feature_dim = features_heads.size()                

        rem0 = (torch.round(embedded_locs/D1) * D1).long()
        deltas = embedded_locs - rem0.float()
        sums = (rem0.sum(1,keepdim = True) // D1)

        #An ugly 2-step process to get the inverse argsort
        sort_idx = torch.argsort(deltas,axis = 1,descending = True)
        ranks = torch.argsort(sort_idx)

        #Sum> 0. decrement coords with smallest delta 
        negative_adjustments = ((sums > 0) * (ranks >= (D1 - sums))).long()

        #Sum <  0. increment coords with largest delta 
        positive_adjustmens = ((sums < 0) * (ranks < (-sums))).long()

        rem0_inplane = rem0 + (positive_adjustmens * D1) - (negative_adjustments * D1)
        ranks_inplane = ranks + (positive_adjustmens * D1) - (negative_adjustments * D1)
        ranks_inplane += sums

        deltas_inplane_sorted = torch.sort(embedded_locs - rem0_inplane.float(),descending = False)[0]

        barycentric_part1 = (deltas_inplane_sorted[:,1:] - deltas_inplane_sorted[:,0:-1]) / (D1)
        barycentric_part0 = 1 - (deltas_inplane_sorted[:,-1] - deltas_inplane_sorted[:,-0]) / (D1)

        #n_points x (D+1) . The barycentric weights of the surrounding lattice points for each location 
        barycentric = torch.cat((barycentric_part0.unsqueeze(1),barycentric_part1),dim = 1)

        enclosing_simplex = (self.canonical_simplex[:,ranks_inplane].permute(1,0,2) + rem0_inplane.unsqueeze(1))

        min_simplex_val,max_simplex_val = enclosing_simplex.min() , enclosing_simplex.max()
        encoding_base = (max_simplex_val - min_simplex_val).item()  + 1

        #max_encoding_val = (encoding_base ** (D1+2))
        #assert max_encoding_val < 2**63  , 'Oh noooooo. max encoding val can not fit into long'
        encoding_vector = encoding_base ** torch.arange(D1,device = enclosing_simplex.device)

        enclosing_simplex_flat = enclosing_simplex.reshape(-1,D1)
        encoded_lattice = (encoding_vector.unsqueeze(0) * (enclosing_simplex_flat  -  min_simplex_val)).sum(-1)
        sorted_unique_lattice,sorted_unique_lattice_inverse = torch.unique(encoded_lattice,sorted= True,return_inverse = True)
        barycentric_indices_X = sorted_unique_lattice_inverse
        barycentric_indices_Y = torch.arange(n_points_per_head,device = enclosing_simplex.device).repeat_interleave(D1)
        barycentric_vals = barycentric.flatten(0)


        encoded_neighbor_deltas = (encoding_vector.unsqueeze(0) * self.neighbor_deltas).sum(-1)

        neighbors = sorted_unique_lattice.unsqueeze(0) + encoded_neighbor_deltas.unsqueeze(1)
        out_of_lattice_points = np.isin(neighbors.cpu().numpy(),sorted_unique_lattice.cpu().numpy(),invert = True,assume_unique = False)
        neighbors[torch.BoolTensor(out_of_lattice_points)] = sorted_unique_lattice.max() + 1
        neighbor_locs = torch.unique(neighbors,sorted= True,return_inverse = True)[1]

        agg_positions_bool = (neighbor_locs < sorted_unique_lattice.size(0))
        agg_positions_indices = agg_positions_bool.nonzero()

        reps = agg_positions_bool.long().sum(1)
        repeated_weights = torch.cat([w.repeat(i) for w,i in zip(self.neighbor_weights,reps)])


        agg_Y = neighbor_locs[agg_positions_bool]
        agg_X = agg_positions_indices[:,1]
        blur_projection = torch.sparse.FloatTensor(torch.stack((agg_X,agg_Y)),repeated_weights)


        if use_custom_op:
            sliced_features = lattice_op_batched.apply(barycentric_indices_X,barycentric_indices_Y,barycentric_vals,
                           blur_projection,features_heads,self.D) 
        else: 
            splat_projections,_,sliced_features = efficient_lattice_batched.filter(barycentric_indices_X,barycentric_indices_Y,barycentric_vals,blur_projection,features_heads,self.D)

            self.splat_projections = splat_projections

        self.blur_projection = blur_projection
        self.sorted_unique_lattice = sorted_unique_lattice

        return sliced_features
       
    @staticmethod
    def filter(barycentric_indices_X,barycentric_indices_Y,barycentric_vals,blur_projection,features_heads,D):
        n_heads,n_points_per_head,feature_dim = features_heads.size()
        head_stride = n_points_per_head * (D  + 1)
        n_unique_lattice_points = blur_projection.size(0)
        splat_projections = [torch.sparse.FloatTensor(torch.stack((barycentric_indices_X[i*head_stride: (i+1) * head_stride] ,barycentric_indices_Y))
                                                      , barycentric_vals[i*head_stride: (i+1) * head_stride],torch.Size([n_unique_lattice_points,n_points_per_head]))  for i in range(n_heads)]
        combined_splatted_features = torch.cat([torch.sparse.mm(splat_projections[i],features_heads[i]) for i in range(n_heads)],dim = 1)

#        unused_lattice_points_fraction = [(splatted_features[i].sum(1) == 0.00).float().mean() for i in range(n_heads)]

        combined_blurred_features = torch.sparse.mm(blur_projection,combined_splatted_features)

        sliced_features = torch.stack([torch.sparse.mm(splat_projections[i].transpose(1,0),combined_blurred_features[:,i * feature_dim: (i+1) * feature_dim]) for i in range(n_heads)])

        
        return splat_projections,combined_blurred_features,sliced_features

    @staticmethod
    def make_embedding_matrix(D):
        basis = torch.triu(torch.ones((D,D)))
        basis[torch.arange(D),torch.arange(D)] = ((torch.arange(D)+1) * -1).float()
        basis = torch.cat((torch.ones((1,D)),basis))
        basis = basis / torch.norm(basis,dim = 0,keepdim = True)
        return basis.transpose(1,0)# * np.sqrt(2.0 / 3.0) * (D + 1)
        

    @staticmethod
    def get_canonical_simplex(D):
        P = torch.arange(D+1).repeat((D+1,1))
        canonical = torch.ones(D+1,D+1).triu(1).flip(0).long() * (-D-1) + P
        return canonical.transpose(1,0)
