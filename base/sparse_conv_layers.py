import torch
import numpy as np
import torch.nn.functional as F
from torch.nn.parameter import Parameter
import torch.nn as nn

from .permutohedral import efficient_lattice_batched


class sparse_conv_attn(nn.Module):
    def __init__(self,D,neighbor_range,loc_scale = 1.0,exp_scale = 1.0):
        super(sparse_conv_attn,self).__init__()

        class _weight_foo(nn.Module):
            def __init__(self,loc_scale,exp_scale):
                super().__init__()
                self.loc_scale = loc_scale
                self.exp_scale = exp_scale

            def forward(self,neighbor_deltas):
                dist = torch.norm(neighbor_deltas.float(),p = 2, dim = -1)
                closeness = torch.exp(- dist * self.exp_scale / self.loc_scale ).float()
                return closeness

        self.weight_foo = _weight_foo(loc_scale,exp_scale)
        self.lattice = efficient_lattice_batched(D,neighbor_range,self.weight_foo)
        self.mask = None
        
    def forward(self,locs,features):
        aug_features =  F.pad(features,(0,1),'constant',1)
        res =  self.lattice(self.weight_foo.loc_scale * locs,aug_features)
        self.normalization = res[:,:,-1:]
        self.normalization = self.normalization + (self.normalization < 1.0e-10).float() * 1.0
        normalized_res = res[:,:,:-1] / self.normalization
        return normalized_res

    def extra_repr(self):
        return 'loc_scale: {} , exp_scale : {}'.format(self.weight_foo.loc_scale,self.weight_foo.exp_scale)

class sparse_conv_attn_dumb(nn.Module):
    def __init__(self,D,exp_scale = 1.0):
        super(sparse_conv_attn_dumb,self).__init__()
        self.exp_scale = exp_scale

    def forward(self,locs,features):
        dist = torch.norm(locs.unsqueeze(1) - locs.unsqueeze(2),p = 2,dim = -1)
        aug_features =  F.pad(features,(0,1),'constant',1)

        closeness = (torch.exp(-dist * self.exp_scale))
        res =  torch.bmm(closeness,aug_features)
        self.normalization = res[:,:,-1:]
        normalized_res = res[:,:,:-1] / self.normalization
        return normalized_res
    
