import torch
import numpy as np
import torch.nn.functional as f
import torch.nn as nn
from torch.nn.parameter import Parameter


import dgl.function as fn
from dgl.nn.pytorch.softmax import edge_softmax
from dgl.nn.pytorch.utils import Identity

from .sparse_conv_layers import sparse_conv_attn,sparse_conv_attn_dumb 


class GATConvEuclidean(nn.Module):
    def __init__(self,
                 in_feats,
                 out_feats,
                 num_heads,
                 loc_dim,
                 feat_drop=0.,
                 attn_drop = 0.,
                 lattice_neighbor_range = 3,
                 global_attention = False,
                 exact_global_attention = False,
                 ignore_structure = False,
                 exp_scale = 10.0,
                 max_lattice_decay = 1.0e-5,
                 residual = False,
                 activation=None):
        super(GATConvEuclidean, self).__init__()
        self._num_heads = num_heads
        self._in_feats = in_feats
        self._out_feats = out_feats
        self.fc = nn.Linear(in_feats, out_feats * num_heads, bias=False)
        self.embedding_matrix = nn.Parameter(torch.FloatTensor(size=(loc_dim, num_heads, out_feats)))
        self.feat_drop = nn.Dropout(feat_drop)
        self.attn_drop = nn.Dropout(attn_drop)

        self.global_attention = global_attention
        self.ignore_structure = ignore_structure
        if global_attention:
           furthest_lattice_point = np.sqrt((lattice_neighbor_range**2) * loc_dim + (lattice_neighbor_range * loc_dim)**2)
           loc_scale = (-exp_scale * furthest_lattice_point) / np.log(max_lattice_decay)
           if exact_global_attention:
               self.sparse_conv_layer = sparse_conv_attn_dumb(loc_dim,exp_scale = exp_scale)               
           else:
               self.sparse_conv_layer = sparse_conv_attn(loc_dim,lattice_neighbor_range,loc_scale = loc_scale,exp_scale = exp_scale)

               


        if residual:
            if in_feats != out_feats:
                self.res_fc = nn.Linear(in_feats, num_heads * out_feats * (2 if (global_attention and not(ignore_structure)) else 1),
                                                  bias=False)
            else:
                self.res_fc = Identity()
        else:
            self.register_buffer('res_fc', None)


        self.reset_parameters()
        self.activation = activation

    def reset_parameters(self):
        """Reinitialize learnable parameters."""
        gain = nn.init.calculate_gain('relu')
        nn.init.xavier_normal_(self.fc.weight, gain=gain)
        nn.init.xavier_normal_(self.embedding_matrix, gain=gain)
        if isinstance(self.res_fc, nn.Linear):
            nn.init.xavier_normal_(self.res_fc.weight, gain=gain)


    def forward(self, graph, feat):
        r"""Compute graph attention network layer.

        Parameters
        ----------
        graph : DGLGraph
            The graph.
        feat : torch.Tensor
            The input feature of shape :math:`(N, D_{in})` where :math:`D_{in}`
            is size of input feature, :math:`N` is the number of nodes.

        Returns
        -------
        torch.Tensor
            The output feature of shape :math:`(N, H, D_{out})` where :math:`H`
            is the number of heads, and :math:`D_{out}` is size of output feature.
        """
        graph = graph.local_var()
        h = self.feat_drop(feat)
        feat = self.fc(h).view(-1, self._num_heads, self._out_feats) #n_nodes x num_heads x n_out_feat
        embeddings = (self.embedding_matrix * feat.unsqueeze(1)).sum(-1) #n_nodes * loc_dim * num_heads
        self.embeddings = embeddings  #For checking and plotting the embeddings later

        graph.ndata.update({'ft': feat, 'emb' : embeddings})

        if not(self.ignore_structure):
            graph.apply_edges(fn.u_sub_v('emb', 'emb', 'emb_diff'))        
            dist_neg = -torch.norm(graph.edata.pop('emb_diff'),p=2,dim = 1) #n_nodes * num_heads
            graph.edata['attn'] = self.attn_drop(edge_softmax(graph, dist_neg)).unsqueeze(-1) #n_nodes * num_heads * 1
            self.attn = graph.edata['attn']
            self.dist_neg = dist_neg
            # message passing
            graph.update_all(fn.u_mul_e('ft', 'attn', 'm'),
                         fn.sum('m', 'ft'))
            rst = graph.ndata['ft']

        if self.global_attention:
           filtered_features = []

           filtered_features = self.sparse_conv_layer(embeddings.permute(2,0,1),feat.permute(1,0,2)).permute(1,0,2)
           
           # for i in range(self._num_heads):
           #     filtered_features.append(self.sparse_conv_layers[i](embeddings[:,:,i],feat[:,i,:])) #n_nodes * n_features
           # filtered_features = torch.stack(filtered_features).permute(1,0,2) #n_nodes * num_heads * n_features
                   

           if self.ignore_structure:
               rst = filtered_features
           else:
               rst = torch.cat((rst,filtered_features),dim = 1)
               

        # residual
        if self.res_fc is not None:
            resval = self.res_fc(h).view(h.shape[0], -1, self._out_feats)
            rst = rst + resval
        # activation
        if self.activation:
            rst = self.activation(rst)
        return rst
        

