# Permutohedral Graph Convolutional Networks

Code for the experiments in the Neurips 2020 submission "Permutohedral-GCN: Graph Convolutional Networks with Global Attention". The main python executable is `main.py`. Results are saved under a `./runs/` directory created at the invocation directory. An invocation of `main.py` will save various accuracy metrics as well as the model parameters in the file `./runs/{model name}_{job idx}`. Accuracy figures as well as several diagnostics are also printed out. 

For the Cora, Citeseer, and Pubmed benchmarks, the graphs are downloaded automatically by the dgl library. The Actor, Wisconsin, Cornell, and Texas datasets are included in the repository and are located under the /datasets/ folder.

### Required Packages
See requirements.txt for a complete list of required Python packages. Install required packages using the command:
```shell
pip install -r requirements.txt
```

### General usage
```shell
main.py [-h] [-m {KGCN,PHGCN}]
               [-f HIDDEN_FEAT_REPR_DIMS [HIDDEN_FEAT_REPR_DIMS ...]]
               [-i ITERATIONS] [--dropout DROPOUT] [--lr LR]
               [--weight-decay WEIGHT_DECAY] [--job-idx JOB_IDX]
               [--log-interval N] [--loglvl LVL] [--disable-cuda]
               [--datasets-path DATASETS_PATH] [--no-self-loop]
               [--dataset DATASET] [--custom-split-file CUSTOM_SPLIT_FILE]
               [--split-seed SPLIT_SEED]
               [--optimizer-restart-interval OPTIMIZER_RESTART_INTERVAL]
               [--original-split] [--homogeneous-split] [--loc-dim LOC_DIM]
               [--lattice-neighbor-range LATTICE_NEIGHBOR_RANGE]
               [--exp-scale EXP_SCALE] [--max-lattice-decay MAX_LATTICE_DECAY]
               [--num-heads NUM_HEADS [NUM_HEADS ...]]
               [--attn-dropout ATTN_DROPOUT] [--residual] [--global-attention]
               [--ignore-structure] [--euclidean-attn]


```
arguments:
```
optional arguments:
  -h, --help            show this help message and exit
  -m {KGCN,PHGCN}, --model {KGCN,PHGCN}
                        graph neural net model (default kgcn)
  -f HIDDEN_FEAT_REPR_DIMS [HIDDEN_FEAT_REPR_DIMS ...], --hidden-feat-repr-dims HIDDEN_FEAT_REPR_DIMS [HIDDEN_FEAT_REPR_DIMS ...]
                        dimension of the hidden feature representation (no
                        input or output)
  -i ITERATIONS, --iterations ITERATIONS
                        number of training iterations
  --dropout DROPOUT     feature dropout
  --lr LR               learning rate
  --weight-decay WEIGHT_DECAY
                        Weight for L2 loss
  --job-idx JOB_IDX     job index provided by the job manager
  --log-interval N, -p N
                        print frequency (default: 10)
  --loglvl LVL          set logging level.
  --disable-cuda        disable cuda
  --datasets-path DATASETS_PATH
                        Path to directory containing the graph datasets
  --no-self-loop        no self-loop graph augmentation
  --dataset DATASET     Dataset name: cora, citeseer, pubmed, film, wisconsin,
                        cornell, texas
  --custom-split-file CUSTOM_SPLIT_FILE
                        file containing custom train,test,val splits.
  --split-seed SPLIT_SEED
                        Seed to use for the 60/20/20 train/validation/test
                        split
  --optimizer-restart-interval OPTIMIZER_RESTART_INTERVAL
                        Interval between optimizer restarts. Set to negative
                        number to disable restarts
  --original-split      Uses the Kipf and Welling original split for cora,
                        citeseer, and pubmed datasets
  --homogeneous-split   Make sure the classes are balanced in the split
  --loc-dim LOC_DIM     Dimensionality of the location(embedding) variables
  --lattice-neighbor-range LATTICE_NEIGHBOR_RANGE
                        The range of lattice points to aggregate from in the
                        lattice. Equivalent to a kernel with lattice point
                        width of (2*lattice_neighbor_range+1)
  --exp-scale EXP_SCALE
                        Scales the embedding distance when calculating global
                        attention. Larger values lead to sharper attention
  --max-lattice-decay MAX_LATTICE_DECAY
                        The maximum exponential decay at the furthest lattice
                        point that we aggregate from.
  --num-heads NUM_HEADS [NUM_HEADS ...]
                        number of attention heads for GAT and PHGCN
  --attn-dropout ATTN_DROPOUT
                        attention dropout for GAT and PHGCN
  --residual            enable residual learning
  --global-attention    enable global attention based on the permutohedral
                        lattice
  --ignore-structure    Skip structural based aggregation
  --euclidean-attn      enable Euclidean attention mechanism. Otherwise,
                        default to original GAT attention mechanism

```

### Performing the experiments in the paper
The YAML file : `all_experiments.yaml`,  contains a list of the invocation commands needed to run the experiments described in the paper.

### Notes

- Code development and all experiments were done with Python 3.6 and pytorch 1.4
