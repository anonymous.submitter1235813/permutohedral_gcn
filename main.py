import logging
from argparse import ArgumentParser
from pathlib import Path
import torch.nn.functional as f
import numpy as np
from copy import deepcopy
import sys

import torch
import os

from models.PHGCN import PHGCN
from models.KGCN import KGCN
from data_utils import load_dataset

logger = logging.getLogger(__name__)

def save_results(state,  filename):
    """Saves checkpoint to disk"""
    directory = "runs/"
    if not os.path.exists(directory):
        os.makedirs(directory)
    filename = directory + filename
    torch.save(state, filename)

def num_params(module, trainable_only=True):
    return sum(p.numel() for p in module.parameters()
               if (p.requires_grad or not trainable_only))


def train_pass(data,model,optimizer):
    features, graph, labels, mask = data
    model.train()
    optimizer.zero_grad()
    logits = model(graph, features)
    loss = model.loss(labels[mask], logits[mask, :])
    loss.backward()
    optimizer.step()
    return loss.item()


def infer_pass(data,model):
    features, graph, labels, mask = data
    model.eval()
    with torch.no_grad():
        logits = model(graph, features)
        targets = labels[mask]
        loss = model.loss(labels[mask], logits[mask, :])
        accuracy = (logits[mask].argmax(1) == targets).float().mean().item()
        return loss.item(),accuracy

def train(model,
          args,
          full_data,
          device,
          log_interval,
          iterations,
          experiment_dir='result'):

    """ Build trainer and evaluator. """

    best_loss = np.inf
    experiment_dir = Path(experiment_dir)

    graph, num_labels,features, labels, train_mask, val_mask, test_mask = full_data
    # de-multiplex data
    train_data = (features, graph, labels, train_mask)
    val_data = (features, graph, labels, val_mask)
    test_data = (features, graph, labels, test_mask)

    best_val_loss = np.inf
    best_model_state_dict = None
    best_iter = 0

    train_metrics = []
    
    optimizer = torch.optim.Adam(
        model.parameters(),
        lr=args.lr,
        weight_decay=args.weight_decay,
    )

    for iter in range(iterations):
        if args.optimizer_restart_interval > 0  and (iter % args.optimizer_restart_interval) == 0:
            optimizer = torch.optim.Adam(
                model.parameters(),
                lr=args.lr,
                weight_decay=args.weight_decay,
            )
            logger.info('******** Restarting optimizer ')
        train_pass(train_data,model,optimizer)

        if (iter % log_interval) == 0:
            train_loss,train_acc = infer_pass(train_data,model)
            val_loss,val_acc = infer_pass(val_data,model)
            train_metrics.append((train_loss,train_acc,val_loss,val_acc))
            
            if val_loss < best_val_loss:
                best_model_state_dict = deepcopy(model.state_dict())
                best_val_loss = val_loss
                best_iter = iter
            
            # send data to logger
            result_message = (
                f"iteration [{iter}/{iterations}] | "
            )
            result_message += ', '.join([
                f"Loss: "
                f"train={train_loss:.4f}, "
                f"valid={val_loss:.4f} "
                f" | "
                f"Accuracy: "
                f"train={train_acc:.4f} "
                f"valid={val_acc:.4f} "
                f" | "
            ])
            logger.info(result_message)
    
    model.load_state_dict(best_model_state_dict)
    test_loss,test_acc  = infer_pass(test_data,model)
    val_loss,val_acc  = infer_pass(val_data,model)    

    logger.info('******** Best validation point at iter : '+repr(best_iter))

    logger.info('******** test loss at best validation point : {}'.format(test_loss))
    logger.info('******** test accuracy at best validation point : {}'.format(test_acc))

    logger.info('******** validation loss at best validation point : {}'.format(val_loss))
    logger.info('******** validation accuracy at best validation point : {}'.format(val_acc))

    
    return test_loss,test_acc,train_metrics
    
    
    
if __name__ == '__main__':
    # setup logging
    stdoutlog = logging.StreamHandler(sys.stdout)
    formatter = logging.Formatter(
        '%(asctime)s.%(msecs)03d %(funcName)-20s %(levelname)-8s %(message)s',
        datefmt='%H:%M:%S',
    )
    stdoutlog.setFormatter(formatter)
    logger.addHandler(stdoutlog)

    parser = ArgumentParser(description="PHGCN graph experiments")
    parser.add_argument(
        "-m", "--model",
        choices=["KGCN", "PHGCN"],
        default="KGCN",
        help="graph neural net model (default kgcn)",
    )
    parser.add_argument(
        "-f", "--hidden-feat-repr-dims",
        default=[16],
        type=int,
        nargs="+",
        help="dimension of the hidden feature representation (no input or output)"
    )
    parser.add_argument(
        "-i", "--iterations",
        default=200,
        type=int,
        help="number of training iterations"
    )

    parser.add_argument(
        "--dropout",
        type=float,
        default=0.5,
        help="feature dropout"
    )
    parser.add_argument(
        "--lr",
        type=float,
        default=1e-2,
        help="learning rate"
    )
    parser.add_argument(
        "--weight-decay",
        type=float,
        default=0.0,
        help="Weight for L2 loss"
    )

    parser.add_argument('--job-idx', default=0, type=int,
                    help='job index provided by the job manager')

    parser.add_argument(
        "--log-interval", "-p", default=10, type=int, metavar="N",
        help="print frequency (default: 10)"
    )
    parser.add_argument(
        "--loglvl", default="INFO", type=str.upper, metavar="LVL",
        help="set logging level."
    )

    parser.add_argument(
        "--disable-cuda", action="store_true",
        help="disable cuda"
    )

    parser.add_argument(
        "--datasets-path",
        type=Path,
        default="./datasets/",
        help="Path to directory containing the graph datasets"
    )

    parser.add_argument(
        "--no-self-loop",
        dest="self_loop",
        action="store_false",
        help="no self-loop graph augmentation"
    )

    parser.add_argument(
        "--dataset",
        type=str,
        default="cora",
        help="Dataset name: cora, citeseer, pubmed, film, wisconsin, cornell, texas ",
    )

    parser.add_argument(
        "--custom-split-file", default="", type=str,
        help="file containing custom train,test,val splits."
    )

    parser.add_argument(
        "--split-seed",
        default=1,
        type=int,
        help="Seed to use for the 60/20/20 train/validation/test split"
    )
    parser.add_argument(
        "--optimizer-restart-interval",
        default=-2,
        type=int,
        help="Interval between optimizer restarts. Set to negative number to disable restarts"
    )

    
    parser.add_argument(
        "--original-split",
        action='store_true',
        help="Uses the Kipf and Welling original split for cora, citeseer, and pubmed datasets"
    )

    parser.add_argument(
        "--homogeneous-split",
        action='store_true',
        help="Make sure the classes are balanced in the split"
    )
    
    
    #PHGCN args 


    parser.add_argument(
        "--loc-dim",
        default=4,
        type=int,
        help="Dimensionality of the location(embedding) variables"
    )

    parser.add_argument(
        "--lattice-neighbor-range",
        default=3,
        type=int,
        help="The range of lattice points to aggregate from in the lattice. Equivalent to a kernel with lattice point width of (2*lattice_neighbor_range+1)"
    )


    parser.add_argument(
        "--exp-scale",
        type=float,
        default= 10.0,
        help="Scales the embedding distance when calculating global attention. Larger values lead to sharper attention "
    )

    parser.add_argument(
        "--max-lattice-decay",
        type=float,
        default= 1.0e-5,
        help="The maximum exponential decay at the furthest lattice point that we aggregate from. "
    )



    parser.add_argument(
        "--num-heads",
        type=int,
        nargs='+',
        default=[8],
        help="number of attention heads for GAT and PHGCN"
    )
    parser.add_argument(
        "--attn-dropout",
        type=float,
        default=0.6,
        help="attention dropout for GAT and PHGCN"
    )
    parser.add_argument(
        "--residual",
        action='store_true',
        help="enable residual learning"
    )

    parser.add_argument(
        "--global-attention",
        action='store_true',
        help="enable global attention based on the permutohedral lattice "
    )

    parser.add_argument(
        "--ignore-structure", action="store_true",
        help="Skip structural based aggregation "
    )
    
    
    parser.add_argument(
        "--euclidean-attn",
        action='store_true',
        help="enable Euclidean attention mechanism. Otherwise, default to original GAT attention mechanism",
    )

    parser.add_argument(
        "--exact-global-attention",
        action='store_true',
        help="Use O(N^2) exact global attention based on Euclidean distances between embeddings"
    )
    
    
    args= parser.parse_args()
    logger.setLevel(args.loglvl)

    use_cuda = torch.cuda.is_available() and not args.disable_cuda
    device = torch.device('cuda' if use_cuda else 'cpu')
    
    full_data = load_dataset(args,device)
    train_mask,val_mask,test_mask = full_data[-3:]
    logger.info('******** train/val/test split : {}/{}/{} : '.format(train_mask.float().mean().item(),val_mask.float().mean().item(),test_mask.float().mean().item()))

    num_labels,features = full_data[1],full_data[2]
    in_feature_dim = features.size(1)

    feat_repr_dims = [in_feature_dim] + args.hidden_feat_repr_dims + [num_labels]

    if args.model == 'KGCN':
        model = KGCN(feat_repr_dims,activation = f.elu,dropout = args.dropout).to(device)
    elif args.model == 'PHGCN':
        if args.global_attention:
            assert args.euclidean_attn, 'Euclidean distance attention must be chosen in order to use lattice based global attention in the PHGCN network'
        if args.ignore_structure:
            assert args.global_attention, 'Global attention based aggregation must be used if graph structure is to be ignored'
            
        model = PHGCN(
            feat_repr_dims,
            args.num_heads,
            activation=f.elu,
            feat_dropout=args.dropout,
            attn_dropout=args.attn_dropout,
            euclidean_attn = args.euclidean_attn,
            loc_dim = args.loc_dim,
            global_attention = args.global_attention,
            exact_global_attention = args.exact_global_attention,            
            ignore_structure = args.ignore_structure,
            lattice_neighbor_range = args.lattice_neighbor_range,
            max_lattice_decay = args.max_lattice_decay,
            exp_scale = args.exp_scale,
            alpha=0.2,
            residual=args.residual,
        ).to(device)
        
    print(args)
    print(model)
    nparams = sum([p.numel() for p in model.parameters() if p.requires_grad])
    print('######nparams : ',nparams)
    # deployment
    
    # start training
    test_loss,test_acc,train_metrics = train(
        model,
        args,
        full_data,
        device = device,
        log_interval = args.log_interval,
        iterations = args.iterations
    )

    results_filename = args.model + '_' + repr(args.job_idx)
    save_results({
        'model_size' : num_params(model),
        'train_metrics' : train_metrics,
        'test_loss' : test_loss,
        'train_mask' : train_mask,
        'test_mask' : test_mask,
        'val_mask' : val_mask,
        'test_acc' : test_acc,
        'state_dic' : model.state_dict(),
        'args' : args},
        filename = results_filename)


